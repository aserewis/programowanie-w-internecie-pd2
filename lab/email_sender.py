# -*- coding: utf-8 -*-


def create_and_send_email():
    """Funkcja interaktywna pytająca użytkownika o potrzebne dane
    i wysyłająca na ich podstawie list elektroniczny.
    """
    # Odczyt danych od użytkownika

    # Stworzenie wiadomości

    try:
        # Połączenie z serwerem pocztowym

        # Ustawienie parametrów

        # Autentykacja

        # Wysłanie wiadomości
        pass

    finally:
        # Zamknięcie połączenia
        pass


if __name__ == '__main__':
    decision = 't'
    while decision in ['t', 'T', 'y', 'Y']:
        create_and_send_email()
        decision = raw_input(u'Wysłać jeszcze jeden list? ')
